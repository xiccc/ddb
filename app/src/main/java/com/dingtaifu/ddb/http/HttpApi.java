package com.dingtaifu.ddb.http;

import com.dingtaifu.ddb.bean.BaseResponse;
import com.dingtaifu.ddb.ui.discover.bean.NewsDetailBean;
import com.dingtaifu.ddb.ui.discover.bean.NewsItemBean;
import com.dingtaifu.ddb.ui.login.bean.UserInfoBean;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/2 21:09
 */

public interface HttpApi {

    //发现 - 获取资讯列表
    @POST("dingdingbang/ddbKnowledge/queryKnowledgeTitle")
    Observable<BaseResponse<List<NewsItemBean>>> getNewsItems();

    //发现 - 获取资讯详情
    @POST("dingdingbang/ddbKnowledge/queryKnowledgeContent")
    Observable<BaseResponse<NewsDetailBean>> getNewsDetail(@Body RequestBody requestBody);


    //发现 - 资讯详情 - 收藏
    @POST("dingdingbang/mybookmark/addDdbMybookmark")
    Observable<BaseResponse> addShouCang(@Body RequestBody requestBody);

    //发现 - 资讯详情 - 取消收藏
    @POST("dingdingbang/mybookmark/removeDdbMybookmark")
    Observable<BaseResponse> delShouCang(@Body RequestBody requestBody);

    //发现 - 资讯详情 - 点赞状态
    @POST("dingdingbang/ddbMyHitbookmark/query")
    Observable<BaseResponse<Integer>> getNewsZanStatus(@Body RequestBody requestBody);

    //发现 - 资讯详情 - 点赞或者取消点赞
    @POST("dingdingbang/ddbKnowledge/updateKnowledgeHit_number")
    Observable<BaseResponse<String>> handleZan(@Body RequestBody requestBody);


    //登录
    @POST("dingdingbang/user/userLogin")
    Observable<BaseResponse<UserInfoBean>> login(@Body RequestBody requestBody);

    //用户认证
    @POST("dingdingbang/user/authentication")
    Observable<BaseResponse<UserInfoBean>> doRealNameAuth(@Body RequestBody requestBody);

    //注册
    @POST("dingdingbang/user/userRegister")
    Observable<BaseResponse> register(@Body RequestBody requestBody);

    //发送验证码
    @POST("dingdingbang/user/sendPhoneCheckCode")
    Observable<BaseResponse<String>> sendPhoneCheckCode(@Body RequestBody requestBody);

}
