package com.dingtaifu.ddb.http;


import com.dingtaifu.ddb.app.MyApp;
import com.dingtaifu.ddb.util.NetUtil;

import java.net.SocketTimeoutException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;


public abstract class HttpSubscriber<T> extends Subscriber<T> {

    public final String ERROR_DEFAULT  = "201";    //默认异常
    public final String ERROR_SERVER   = "500";    //服务器异常
    public final String ERROR_NETWORK  = "-4";     //无网络
    public final String ERROR_TIME_OUT = "-5";     //请求超时

    public HttpSubscriber() {
    }

    @Override
    public void onStart() {
        super.onStart();
        _onStart();
    }

    @Override
    public void onCompleted() {
        _onCompleted();
    }

    @Override
    public void onNext(T t) {
        _onNext(t);
    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
        if (!NetUtil.isConnected(MyApp.getContext())) {
            _onError("网络不可用", ERROR_NETWORK);
        } else if (e instanceof ApiException) {
            //自定义异常 处理自己服务器的 如规定的登录失效可在此处理
            //            if (((ApiException) e).getCode() == ERROR_NOT_LOGIN) {
            //                EventBus.getDefault().post(new LogoutEvent(App.getContext(), 0));
            //                _onError("请先登录", ((ApiException) e).getCode());
            //            } else {
            _onError(e.getMessage(), ((ApiException) e).getCode());
            //            }
        } else if (e instanceof HttpException) {
            HttpException exception = (HttpException) e;
            if (exception.code() == 500) {
                _onError("服务器异常，请稍后重试", ERROR_SERVER);
            } else {
                _onError("连接服务器失败，请稍后再试", ERROR_DEFAULT);
            }
        } else if (e instanceof SocketTimeoutException) {
            _onError("连接服务器超时，请稍后再试", ERROR_TIME_OUT);
        } else {
            _onError("连接服务器失败，请稍后再试", ERROR_DEFAULT);
        }
        _onCompleted();
    }

    protected abstract void _onStart();

    protected abstract void _onNext(T t);

    protected abstract void _onError(String message);

    protected abstract void _onCompleted();

    protected void _onError(String message, String code) {
        _onError(message);
    }

}
