package com.dingtaifu.ddb.app;

import android.app.Application;
import android.content.Context;

import com.dingtaifu.ddb.config.AccountHelper;
import com.dingtaifu.ddb.config.ConfigUtil;
import com.dingtaifu.ddb.util.ViewUtil;

public class MyApp extends Application {

    public static MyApp mMyApp;

    @Override
    public void onCreate() {
        super.onCreate();
        mMyApp = this;
        initConfig();
    }

    private void initConfig() {
        //加载布局
        ViewUtil.initLoadingView(getContext());
        //账号
        AccountHelper.getInstance().init();
    }

    public static Context getContext() {
        return mMyApp.getApplicationContext();
    }

    //保存一些常用的配置
    private static ConfigUtil configUtil = null;

    public static ConfigUtil getConfig() {
        if (configUtil == null) {
            configUtil = new ConfigUtil();
        }
        return configUtil;
    }

}
