package com.dingtaifu.ddb.event;

import com.dingtaifu.ddb.ui.login.bean.UserInfoBean;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/5 14:54
 */

public class RealNameAuthEvent {

    private UserInfoBean userInfoBean;

    public RealNameAuthEvent(UserInfoBean userInfoBean) {
        this.userInfoBean = userInfoBean;
    }

    public UserInfoBean getUserInfoBean() {
        return userInfoBean;
    }

    public void setUserInfoBean(UserInfoBean userInfoBean) {
        this.userInfoBean = userInfoBean;
    }
}
