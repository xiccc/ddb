package com.dingtaifu.ddb.util;

import android.util.Log;

import com.alibaba.fastjson.JSONObject;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/5/9 12:23
 */

public class ConvertUtil {

    public static String toJsonString(Object object) {

        String result = "";

        try {

            result = JSONObject.toJSONString(object);

        } catch (Exception e) {
            // TODO: handle exception
        }

        return result;
    }

    public static <T> T toObject(String json, Class<T> clazz) {

        T instance_class = null;

        try {
            instance_class = JSONObject.parseObject(json, clazz);
        } catch (Exception e) {
            Log.e("ConvertUtil:toObject:", e.getMessage());
        }

        return instance_class;
    }
}
