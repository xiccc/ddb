package com.dingtaifu.ddb.util;

import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/3/29 15:23
 */

public class Tools {

    /**
     * 时间戳转时间
     */
    public static String parseTime(String time) {
        if (TextUtils.isEmpty(time)) {
            return "";
        }
        SimpleDateFormat sdr = new SimpleDateFormat("yyyy-MM-dd");
        @SuppressWarnings("unused")
        long lcc = Long.valueOf(time);
        int i = Integer.parseInt(time);
        return sdr.format(new Date(i * 1000L));
    }

    /**
     * 把long 转换成 日期 再转换成String类型
     */
    public static String parseTime(Long millSec) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(millSec);
        return sdf.format(date);
    }


    /**
     * 判断是否连续点击
     * 对于 startActivity 设置 singletop 无效果
     * 则这样 防止 连续点击跳重复界面
     */
    private static long lastClickTime;

    public static boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        if (time - lastClickTime < 500) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

}
