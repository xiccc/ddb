package com.dingtaifu.ddb.util;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.dingtaifu.ddb.R;

public class TitleUtil {

    private Toolbar mToolbar;

    private TextView mTitleBigText;
    private TextView mTitleSmallText;

    private AppCompatActivity mActivity;

    //activity构造
    public TitleUtil(AppCompatActivity activity, View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (mToolbar == null) {
            return;
        }
        mTitleBigText = (TextView) view.findViewById(R.id.tv_big_content);
        mTitleSmallText = (TextView) view.findViewById(R.id.tv_small_content);
        this.mActivity = activity;
        mActivity.setSupportActionBar(mToolbar);
        mActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void setTitleBigText(String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        mTitleBigText.setText(text);
    }

    public void setTitleSmallText(String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        mTitleSmallText.setText(text);
    }
}
