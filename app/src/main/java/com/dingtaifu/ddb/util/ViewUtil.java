package com.dingtaifu.ddb.util;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dingtaifu.ddb.R;


public class ViewUtil {

    public static final String LOADING_TIP = "正在加载...";

    /*********
     * 获取屏幕宽度
     * @param context
     * @return
     */
    public static int getScreenWidth(Context context) {
        DisplayMetrics metric = context.getResources().getDisplayMetrics();
        int width = metric.widthPixels;     // 屏幕宽度（像素）
        return width;
    }

    /***********
     * 获取屏幕高度
     * @param context
     * @return
     */
    public static int getScreenHeight(Context context) {
        DisplayMetrics metric = context.getResources().getDisplayMetrics();
        int height = metric.heightPixels;     // 屏幕高度（像素）
        return height;
    }

    /******
     * 获取状态栏高度
     * @param context
     * @return
     */
    public static int getStatusBarH(Context context) {
        int statusHeight = -1;
        try {
            Class<?> clazz = Class.forName("com.android.internal.R$dimen");
            Object object = clazz.newInstance();
            int height = Integer.parseInt(clazz.getField("status_bar_height")
                    .get(object).toString());
            statusHeight = context.getResources().getDimensionPixelSize(height);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statusHeight;
    }


    public static void initLoadingView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_loan_loading, null);
        ViewUtil.setLoadingView(context, view);
    }

    public static void loadingDefault(Activity activity) {
        ViewUtil.setLoadingView(activity, null);
        ViewUtil.showLoadingView(activity, "");
    }

    public static void loadingContent(Activity activity, String content) {
        ViewUtil.setLoadingView(activity, null);
        ViewUtil.showLoadingView(activity, content);
    }

    public static void hideLoading() {
        hideLoadingView();
    }


    //自定义内容加载提示窗
    private static AlertDialog loadingDialog;
    private static View        loadingView;

    public static void showLoadingView(Activity activity, String content) {
        hideLoading();
        loadingDialog = new AlertDialog.Builder(activity, R.style.alert_dialog).create();
        loadingDialog.setCancelable(false);
        //		loadingDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        if (!activity.isFinishing())
            loadingDialog.show();
        //		hideSystemUI(loadingDialog.getWindow().getDecorView());
        //		loadingDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        Window window = loadingDialog.getWindow();
        if (window == null) {
            return;
        }
        if (loadingView != null) {
            window.setContentView(R.layout.layout_loading);
            LinearLayout view = (LinearLayout) window.findViewById(R.id.loading_content);
            view.addView(loadingView);
        } else {
            window.setContentView(R.layout.layout_dialog_progress);
            //适配大小
            int screenWidth = getScreenWidth(activity);
            TextView contentView = (TextView) window.findViewById(R.id.contentView);
            contentView.setText(content);
        }
    }

    public static void setLoadingView(Context context, View view) {
        loadingView = view;
    }

    /*******
     * 关闭loading
     */
    public static void hideLoadingView() {
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                try {
                    loadingDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (loadingView != null) {
                ((LinearLayout) (loadingDialog.getWindow().findViewById(R.id.loading_content))).removeAllViews();
            }
        }
        loadingDialog = null;
    }

}
