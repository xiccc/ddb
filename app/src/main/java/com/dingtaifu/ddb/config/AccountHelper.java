package com.dingtaifu.ddb.config;

import com.dingtaifu.ddb.ui.login.bean.UserInfoBean;
import com.dingtaifu.ddb.util.ConvertUtil;
import com.dingtaifu.ddb.util.SPUtils;


/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/5/9 12:14
 */

public class AccountHelper {

    private boolean isLogin = false;

    private AccountHelper() {
    }

    private static class AccountHelperHolder {
        private static final AccountHelper INSTANCE = new AccountHelper();
    }

    public static final AccountHelper getInstance() {
        return AccountHelperHolder.INSTANCE;
    }


    public void init() {
        setUserInfo(getUserInfo());
    }

    public void setUserInfo(UserInfoBean userInfo) {
        isLogin = userInfo != null;
        SPUtils.getInstance().put(Constant.SPKey.PREFERENCE_KEY_USER_INFO, ConvertUtil.toJsonString(userInfo));
    }

    public UserInfoBean getUserInfo() {
        return ConvertUtil.toObject(SPUtils.getInstance().getString(Constant.SPKey.PREFERENCE_KEY_USER_INFO)
                , UserInfoBean.class);
    }

    public boolean getLoginStatus() {
        return isLogin;
    }

    public void clearUserInfo() {
        setUserInfo(null);
        SPUtils.getInstance().remove(Constant.SPKey.PREFERENCE_KEY_USER_INFO);
    }

}
