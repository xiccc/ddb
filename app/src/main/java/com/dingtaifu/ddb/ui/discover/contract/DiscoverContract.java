package com.dingtaifu.ddb.ui.discover.contract;

import com.dingtaifu.ddb.base.BaseView;
import com.dingtaifu.ddb.ui.discover.bean.NewsItemBean;

import java.util.List;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/3 10:49
 */

public interface DiscoverContract {
    interface View extends BaseView {
        void onGetNewsSuccess(List<NewsItemBean> newsItemBeans);
    }

    interface Presenter {
        void getNewsItems();
    }
}
