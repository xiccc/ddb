package com.dingtaifu.ddb.ui.user.contract;

import com.dingtaifu.ddb.base.BaseView;
import com.dingtaifu.ddb.ui.login.bean.UserInfoBean;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/5 14:23
 */

public interface RealNameAuthContract {
    interface View extends BaseView {
        void realNameAuthSuccess(UserInfoBean userInfoBean);
    }

    interface Presenter {
        void doRealNameAuth(String user_phone,
                            String user_email,
                            String user_name,
                            String user_ID_Card);
    }
}
