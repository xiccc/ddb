package com.dingtaifu.ddb.ui.user.presenter;

import com.dingtaifu.ddb.base.BasePresenter;
import com.dingtaifu.ddb.http.HttpManager;
import com.dingtaifu.ddb.http.HttpSubscriber;
import com.dingtaifu.ddb.ui.login.bean.UserInfoBean;
import com.dingtaifu.ddb.ui.user.contract.RealNameAuthContract;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/5 14:29
 */

public class RealNameAuthPresenter extends BasePresenter<RealNameAuthContract.View> implements RealNameAuthContract
        .Presenter {

    @Override
    public void doRealNameAuth(String user_phone, String user_email, String user_name, String user_ID_Card) {
        JSONObject js = new JSONObject();
        try {
            js.put("user_phone", user_phone);
            js.put("user_email", user_email);
            js.put("user_name", user_name);
            js.put("user_ID_Card", user_ID_Card);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType mediaType = MediaType.parse("application/json; charset=UTF-8");
        RequestBody requestBody = RequestBody.create(mediaType, js.toString());

        toSubscribe(HttpManager.getApi().doRealNameAuth(requestBody), new HttpSubscriber<UserInfoBean>() {
            @Override
            protected void _onStart() {
                mView.showLoading("正在认证...");
            }

            @Override
            protected void _onNext(UserInfoBean userInfoBean) {
                if (userInfoBean != null) {
                    mView.realNameAuthSuccess(userInfoBean);
                } else {
                    _onError("获取认证信息失败！");
                }
            }

            @Override
            protected void _onError(String message) {
                mView.showErrorMsg(message, null);
            }

            @Override
            protected void _onCompleted() {
                mView.stopLoading();
            }
        });
    }
}
