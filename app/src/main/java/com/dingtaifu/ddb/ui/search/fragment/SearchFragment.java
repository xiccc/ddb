package com.dingtaifu.ddb.ui.search.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.base.BaseFragment;
import com.dingtaifu.ddb.config.AccountHelper;
import com.dingtaifu.ddb.ui.search.ReportActivity;
import com.dingtaifu.ddb.ui.user.activity.RealNameAuthActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 首页 - 查询
 */

public class SearchFragment extends BaseFragment {

    @BindView(R.id.tv_big_content)
    TextView tvBigTitle;

    @BindView(R.id.tv_small_content)
    TextView tvSmallTitle;

    @BindView(R.id.borrowing) //借贷
            ImageView mBorrowing;
    @BindView(R.id.accumulation) //公积金
            ImageView mAccumulation;
    @BindView(R.id.enterprise) //企业
            ImageView mEnterprise;
    @BindView(R.id.property) //房产
            ImageView mProperty;
    @BindView(R.id.car) //车辆
            ImageView mCar;
    @BindView(R.id.credit) //征信
            ImageView mCredit;
    @BindView(R.id.evaluation) //反欺诈
            ImageView mEvaluation;
    @BindView(R.id.protection) //社保
            ImageView mProtection;


    public static SearchFragment fragment;

    public static SearchFragment getInstance() {
        if (fragment == null)
            fragment = new SearchFragment();
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_search;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void loadData() {
        tvBigTitle.setText("查询");
        tvSmallTitle.setText("2018/06/09 星期六");
    }

    @OnClick({R.id.tv_big_content, R.id.tv_small_content, R.id.borrowing, R.id.accumulation, R.id.enterprise, R.id.property, R.id.credit, R.id.evaluation, R.id.protection, R.id.car})
    public void onViewClicked(View view) {
        if (AccountHelper.getInstance().getUserInfo().getUser_status().equals("0")) {
            startActivity(new Intent(mActivity, RealNameAuthActivity.class));
            return;
        }
        Intent mIntent = new Intent(getActivity(), ReportActivity.class);
        switch (view.getId()) {
            case R.id.borrowing:
                break;
            case R.id.accumulation:
                break;
            case R.id.enterprise:
                break;
            case R.id.property:
                break;
            case R.id.credit:
                break;
            case R.id.evaluation:
                break;
            case R.id.protection:
                break;
            case R.id.car:
                break;
            default:
                break;
        }
        if (mIntent != null)
            startActivity(mIntent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fragment = null;
    }
}
