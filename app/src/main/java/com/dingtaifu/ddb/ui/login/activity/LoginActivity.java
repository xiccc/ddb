package com.dingtaifu.ddb.ui.login.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.base.BaseActivity;
import com.dingtaifu.ddb.config.AccountHelper;
import com.dingtaifu.ddb.ui.login.bean.UserInfoBean;
import com.dingtaifu.ddb.ui.login.contract.LoginContract;
import com.dingtaifu.ddb.ui.login.presenter.LoginPresenter;
import com.dingtaifu.ddb.ui.main.MainActivity;
import com.dingtaifu.ddb.util.RegexUtils;
import com.dingtaifu.ddb.util.ViewUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 登录
 */
public class LoginActivity extends BaseActivity<LoginPresenter> implements LoginContract.View {

    @BindView(R.id.et_username)
    EditText etUsername;

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.tv_login)
    TextView tvLogin;

    @BindView(R.id.tv_register)
    TextView tvRegister;

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void initPresenter() {
        mPresenter.init(this);
    }

    @Override
    public void loadData() {

    }

    @OnClick({R.id.tv_login, R.id.tv_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_login:
                String username = etUsername.getText().toString().trim();
                String password = etPassword.getText().toString().trim();

                if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                    toast("账号或密码不能为空！");
                } else if (!RegexUtils.isMobileExact(username)) {
                    toast("手机号格式不正确！");
                } else {
                    mPresenter.login(username, password);
                }
                break;
            case R.id.tv_register:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }

    @Override
    public void showLoading(String content) {
        ViewUtil.loadingContent(this, content);
    }

    @Override
    public void stopLoading() {
        ViewUtil.hideLoading();
    }

    @Override
    public void showErrorMsg(String msg, String type) {
        toast(msg);
    }

    @Override
    public void loginSuccess(UserInfoBean userInfoBean) {
        toast(userInfoBean.toString());
        AccountHelper.getInstance().setUserInfo(userInfoBean);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

}
