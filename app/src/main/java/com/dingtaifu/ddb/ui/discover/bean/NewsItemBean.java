package com.dingtaifu.ddb.ui.discover.bean;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/3 10:53
 */

public class NewsItemBean {

    /**
     * knowledge_id : 12
     * title : 微信小程序被曝光“卖假货” 官方已下架875个小程序
     * hit_number : 2
     * type : 0
     */

    private String knowledge_id;
    private String title;
    private String hit_number;
    private String type;

    public String getKnowledge_id() {
        return knowledge_id;
    }

    public void setKnowledge_id(String knowledge_id) {
        this.knowledge_id = knowledge_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHit_number() {
        return hit_number;
    }

    public void setHit_number(String hit_number) {
        this.hit_number = hit_number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
