package com.dingtaifu.ddb.ui.login.presenter;

import com.dingtaifu.ddb.base.BasePresenter;
import com.dingtaifu.ddb.http.HttpManager;
import com.dingtaifu.ddb.http.HttpSubscriber;
import com.dingtaifu.ddb.ui.login.bean.UserInfoBean;
import com.dingtaifu.ddb.ui.login.contract.LoginContract;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/3 16:17
 */

public class LoginPresenter extends BasePresenter<LoginContract.View> implements LoginContract.Presenter {

    @Override
    public void login(String user_phone, String user_password) {

        JSONObject js = new JSONObject();
        try {
            js.put("user_phone", user_phone);
            js.put("user_password", user_password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType mediaType = MediaType.parse("application/json; charset=UTF-8");
        RequestBody requestBody = RequestBody.create(mediaType, js.toString());

        toSubscribe(HttpManager.getApi().login(requestBody), new HttpSubscriber<UserInfoBean>() {
            @Override
            protected void _onStart() {
                mView.showLoading("正在登陆...");
            }

            @Override
            protected void _onNext(UserInfoBean userInfoBean) {
                if (userInfoBean != null) {
                    mView.loginSuccess(userInfoBean);
                } else {
                    _onError("获取用户登陆信息失败！");
                }
            }

            @Override
            protected void _onError(String message) {
                mView.showErrorMsg(message, null);
            }

            @Override
            protected void _onCompleted() {
                mView.stopLoading();
            }
        });
    }
}
