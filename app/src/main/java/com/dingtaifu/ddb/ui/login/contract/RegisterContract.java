package com.dingtaifu.ddb.ui.login.contract;

import com.dingtaifu.ddb.base.BaseView;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/4 10:23
 */

public interface RegisterContract {
    interface View extends BaseView {
        void registerSuccess();

        void sendCheckCodeSuccess(String code_key);
    }

    interface Presenter {
        void register(String user_phone,
                      String user_password,
                      String user_invitation_code,
                      String code_key,
                      String code_value);

        void sendPhoneCheckCode(String phone);
    }

}
