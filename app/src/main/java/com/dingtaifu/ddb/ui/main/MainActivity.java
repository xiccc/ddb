package com.dingtaifu.ddb.ui.main;

import android.view.KeyEvent;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.app.AppManager;
import com.dingtaifu.ddb.base.BaseActivity;

import butterknife.BindView;

public class MainActivity extends BaseActivity {

    @BindView(R.id.container)
    FrameLayout mContainer;

    @BindView(R.id.group)
    RadioGroup mGroup;

    private FragmentFactory.FragmentStatus toTabIndex = FragmentFactory.FragmentStatus.None;
    private int                            oldCheckId = R.id.rb_search;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void loadData() {
        mGroup.setOnCheckedChangeListener(changeListener);
        //首页默认展现【查询】模块
        check(FragmentFactory.FragmentStatus.Search);
    }


    RadioGroup.OnCheckedChangeListener changeListener = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case R.id.rb_search:
                    toTabIndex = FragmentFactory.FragmentStatus.Search;
                    changeTab(FragmentFactory.FragmentStatus.Search);
                    oldCheckId = R.id.rb_search;
                    break;
                case R.id.rb_discover:
                    toTabIndex = FragmentFactory.FragmentStatus.Discover;
                    changeTab(FragmentFactory.FragmentStatus.Discover);
                    oldCheckId = R.id.rb_discover;
                    break;
                case R.id.rb_service:
                    toTabIndex = FragmentFactory.FragmentStatus.Service;
                    changeTab(FragmentFactory.FragmentStatus.Service);
                    oldCheckId = R.id.rb_service;
                    break;
                case R.id.rb_my:
                    toTabIndex = FragmentFactory.FragmentStatus.My;
                    changeTab(FragmentFactory.FragmentStatus.My);
                    oldCheckId = R.id.rb_my;
                    break;
                default:
                    break;

            }
        }
    };


    /***********
     * 获取所选状态的checkId
     */
    public int getCheckIdByStatus(FragmentFactory.FragmentStatus status) {
        int id = R.id.rb_search;
        switch (status) {
            case Search:
                id = R.id.rb_search;
                break;
            case Discover:
                id = R.id.rb_discover;
                break;
            case Service:
                id = R.id.rb_service;
                break;
            case My:
                id = R.id.rb_my;
                break;
            default:
                break;
        }
        return id;
    }

    public void check(FragmentFactory.FragmentStatus status) {
        mGroup.check(getCheckIdByStatus(status));
    }

    /***********
     * 切换导航栏
     */
    private FragmentFactory.FragmentStatus odlState = FragmentFactory.FragmentStatus.None;

    public void changeTab(FragmentFactory.FragmentStatus status) {
        if (status == odlState) {
            return;
        }
        FragmentFactory.changeFragment(getSupportFragmentManager(), status, R.id.container);
        odlState = status;
    }

    public RadioGroup getGroup() {
        return mGroup;
    }


    @Override
    protected void onResume() {
        super.onResume();
        mGroup.setOnCheckedChangeListener(changeListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * 两次返回退出
     */
    private long mExitTime;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                mExitTime = System.currentTimeMillis();
            } else {
                AppManager.getInstance().AppExit(this);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
