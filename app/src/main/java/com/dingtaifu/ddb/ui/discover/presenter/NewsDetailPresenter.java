package com.dingtaifu.ddb.ui.discover.presenter;

import com.dingtaifu.ddb.base.BasePresenter;
import com.dingtaifu.ddb.http.HttpManager;
import com.dingtaifu.ddb.http.HttpSubscriber;
import com.dingtaifu.ddb.ui.discover.bean.NewsDetailBean;
import com.dingtaifu.ddb.ui.discover.contract.NewsDetailContract;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/5 10:13
 */

public class NewsDetailPresenter extends BasePresenter<NewsDetailContract.View> implements NewsDetailContract
        .Presenter {

    @Override
    public void getNewsDetail(String user_Id, String knowledge_id, final boolean isRefresh) {

        JSONObject js = new JSONObject();
        try {
            js.put("user_Id", user_Id);
            js.put("knowledge_id", knowledge_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType mediaType = MediaType.parse("application/json; charset=UTF-8");
        RequestBody requestBody = RequestBody.create(mediaType, js.toString());

        toSubscribe(HttpManager.getApi().getNewsDetail(requestBody), new HttpSubscriber<NewsDetailBean>() {
            @Override
            protected void _onStart() {
                mView.showLoading("加载中...");
            }

            @Override
            protected void _onNext(NewsDetailBean newsDetailBean) {
                if (newsDetailBean != null) {
                    mView.getDetailSuccess(newsDetailBean, isRefresh);
                } else {
                    _onError("获取资讯详情失败！");
                }
            }

            @Override
            protected void _onError(String message) {
                mView.showErrorMsg(message, null);
            }

            @Override
            protected void _onCompleted() {
                mView.stopLoading();
            }
        });

    }

    @Override
    public void addShouCang(String user_Id, String knowledge_id) {

        JSONObject js = new JSONObject();
        try {
            js.put("user_Id", user_Id);
            js.put("knowledge_id", knowledge_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType mediaType = MediaType.parse("application/json; charset=UTF-8");
        RequestBody requestBody = RequestBody.create(mediaType, js.toString());

        toSubscribe(HttpManager.getApi().addShouCang(requestBody), new HttpSubscriber() {
            @Override
            protected void _onStart() {
                mView.showLoading("请稍后...");
            }

            @Override
            protected void _onNext(Object o) {
                mView.addShouCangSuccess();
            }

            @Override
            protected void _onError(String message) {
                mView.showErrorMsg(message, null);
            }

            @Override
            protected void _onCompleted() {
                mView.stopLoading();
            }
        });
    }

    @Override
    public void delShouCang(String user_Id, String knowledge_id) {
        JSONObject js = new JSONObject();
        try {
            js.put("user_Id", user_Id);
            js.put("knowledge_id", knowledge_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType mediaType = MediaType.parse("application/json; charset=UTF-8");
        RequestBody requestBody = RequestBody.create(mediaType, js.toString());

        toSubscribe(HttpManager.getApi().delShouCang(requestBody), new HttpSubscriber() {
            @Override
            protected void _onStart() {
                mView.showLoading("请稍后...");
            }

            @Override
            protected void _onNext(Object o) {
                mView.delShouCangSuccess();
            }

            @Override
            protected void _onError(String message) {
                mView.showErrorMsg(message, null);
            }

            @Override
            protected void _onCompleted() {
                mView.stopLoading();
            }
        });
    }

    @Override
    public void getNewsZanStatus(String user_Id, String knowledge_id) {
        JSONObject js = new JSONObject();
        try {
            js.put("user_Id", user_Id);
            js.put("knowledge_id", knowledge_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType mediaType = MediaType.parse("application/json; charset=UTF-8");
        RequestBody requestBody = RequestBody.create(mediaType, js.toString());

        toSubscribe(HttpManager.getApi().getNewsZanStatus(requestBody), new HttpSubscriber<Integer>() {
            @Override
            protected void _onStart() {
                //                mView.showLoading("请稍后...");
            }

            @Override
            protected void _onNext(Integer status) {
                mView.getZanStatus(status);
            }

            @Override
            protected void _onError(String message) {
                mView.showErrorMsg(message, null);
            }

            @Override
            protected void _onCompleted() {
                //                mView.stopLoading();
            }
        });
    }

    @Override
    public void handleZan(String user_Id, String knowledge_id) {
        JSONObject js = new JSONObject();
        try {
            js.put("user_Id", user_Id);
            js.put("knowledge_id", knowledge_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType mediaType = MediaType.parse("application/json; charset=UTF-8");
        RequestBody requestBody = RequestBody.create(mediaType, js.toString());

        toSubscribe(HttpManager.getApi().handleZan(requestBody), new HttpSubscriber<String>() {
            @Override
            protected void _onStart() {
                mView.showLoading("请稍后...");
            }

            @Override
            protected void _onNext(String msg) {
                mView.handleZanSuccess(msg);
            }

            @Override
            protected void _onError(String message) {
                mView.showErrorMsg(message, null);
            }

            @Override
            protected void _onCompleted() {
                mView.stopLoading();
            }
        });
    }
}
