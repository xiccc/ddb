package com.dingtaifu.ddb.ui.discover.contract;

import com.dingtaifu.ddb.base.BaseView;
import com.dingtaifu.ddb.ui.discover.bean.NewsDetailBean;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/5 10:08
 */

public interface NewsDetailContract {

    interface View extends BaseView {
        void getDetailSuccess(NewsDetailBean newsDetailBean, boolean isRefresh);

        void addShouCangSuccess();

        void delShouCangSuccess();

        void getZanStatus(Integer statusZan);

        void handleZanSuccess(String msg);
    }

    interface Presenter {
        void getNewsDetail(String user_Id, String knowledge_id, boolean isRefresh);

        void addShouCang(String user_Id, String knowledge_id);

        void delShouCang(String user_Id, String knowledge_id);

        void getNewsZanStatus(String user_Id, String knowledge_id);

        void handleZan(String user_Id, String knowledge_id);
    }

}
