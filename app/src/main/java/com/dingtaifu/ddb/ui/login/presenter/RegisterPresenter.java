package com.dingtaifu.ddb.ui.login.presenter;

import com.dingtaifu.ddb.base.BasePresenter;
import com.dingtaifu.ddb.http.HttpManager;
import com.dingtaifu.ddb.http.HttpSubscriber;
import com.dingtaifu.ddb.ui.login.contract.RegisterContract;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/4 10:31
 */

public class RegisterPresenter extends BasePresenter<RegisterContract.View> implements RegisterContract.Presenter {

    @Override
    public void register(String user_phone, String user_password, String user_invitation_code,
                         String code_key, String code_value) {
        JSONObject js = new JSONObject();
        try {
            js.put("user_phone", user_phone);
            js.put("user_password", user_password);
            js.put("user_invitation_code", user_invitation_code);
            js.put("code_key", code_key);
            js.put("code_value", code_value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType mediaType = MediaType.parse("application/json; charset=UTF-8");
        RequestBody requestBody = RequestBody.create(mediaType, js.toString());

        toSubscribe(HttpManager.getApi().register(requestBody), new HttpSubscriber() {
            @Override
            protected void _onStart() {
                mView.showLoading("正在注册...");
            }

            @Override
            protected void _onNext(Object o) {
                mView.registerSuccess();
            }

            @Override
            protected void _onError(String message) {
                mView.showErrorMsg(message, null);
            }

            @Override
            protected void _onCompleted() {
                mView.stopLoading();
            }
        });
    }

    @Override
    public void sendPhoneCheckCode(String phone) {

        JSONObject js = new JSONObject();
        try {
            js.put("phone", phone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType mediaType = MediaType.parse("application/json; charset=UTF-8");
        RequestBody requestBody = RequestBody.create(mediaType, js.toString());

        toSubscribe(HttpManager.getApi().sendPhoneCheckCode(requestBody), new HttpSubscriber<String>() {
            @Override
            protected void _onStart() {
                mView.showLoading("请稍后...");
            }

            @Override
            protected void _onNext(String s) {
                mView.sendCheckCodeSuccess(s);
            }

            @Override
            protected void _onError(String message) {
                mView.showErrorMsg(message, null);
            }

            @Override
            protected void _onCompleted() {
                mView.stopLoading();
            }
        });
    }

}
