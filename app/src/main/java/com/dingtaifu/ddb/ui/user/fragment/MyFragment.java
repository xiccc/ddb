package com.dingtaifu.ddb.ui.user.fragment;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.base.BaseFragment;
import com.dingtaifu.ddb.config.AccountHelper;
import com.dingtaifu.ddb.event.RealNameAuthEvent;
import com.dingtaifu.ddb.ui.login.bean.UserInfoBean;
import com.dingtaifu.ddb.ui.user.activity.RealNameAuthActivity;
import com.dingtaifu.ddb.ui.user.adapter.MenuIconAdapter;
import com.dingtaifu.ddb.ui.user.bean.MenuIconBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 首页 - 我的
 */

public class MyFragment extends BaseFragment {

    @BindView(R.id.iv_userimg)
    ImageView ivUserImg;

    @BindView(R.id.iv_setting)
    ImageView ivSetting;

    @BindView(R.id.iv_doauth)
    ImageView ivDoAuth;

    @BindView(R.id.tv_userphone)
    TextView tvUserPhone;

    @BindView(R.id.tv_status)
    TextView tvStatus;

    @BindView(R.id.rv_icon)
    RecyclerView rvIcon;


    public static MyFragment      fragment;
    private       MenuIconAdapter menuIconAdapter;

    public static MyFragment getInstance() {
        if (fragment == null)
            fragment = new MyFragment();
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_my;
    }

    @Override
    public void initPresenter() {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onScanSuccess(RealNameAuthEvent realNameAuthEvent) {
        setUserRealNameAuthStatus(realNameAuthEvent.getUserInfoBean().getUser_status());
    }

    @Override
    public void loadData() {
        EventBus.getDefault().register(this);
        initView();
        initListener();
    }

    private void initListener() {
        menuIconAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                String name = menuIconAdapter.getData().get(position).getName();
                toast(name);
            }
        });
    }


    @OnClick({R.id.iv_doauth, R.id.iv_setting})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_doauth:
                startActivity(new Intent(mActivity, RealNameAuthActivity.class));
                break;
            case R.id.iv_setting:
                toast("设置");
                break;
        }
    }

    private void initView() {
        if (!AccountHelper.getInstance().getLoginStatus()) {
            toast("登陆状态失效 请重新登陆！");
            return;
        }
        rvIcon.setLayoutManager(new GridLayoutManager(mContext, 2));
        menuIconAdapter = new MenuIconAdapter(getMenuIcon(true));
        rvIcon.setAdapter(menuIconAdapter);

        //显示个人数据
        UserInfoBean userInfo = AccountHelper.getInstance().getUserInfo();
        Glide.with(mContext)
                .load(R.mipmap.maorentouxian)
                .apply(new RequestOptions()
                        .error(R.mipmap.maorentouxian)
                        .placeholder(R.mipmap.maorentouxian))
                .into(ivUserImg);

        tvUserPhone.setText(userInfo.getUser_phone());
        setUserRealNameAuthStatus(userInfo.getUser_status());

    }

    /**
     * 认证状态显示不同布局
     */
    private void setUserRealNameAuthStatus(String user_status) {
        if (TextUtils.equals("0", user_status)) {
            tvStatus.setText("未实名认证");
            ivDoAuth.setVisibility(View.VISIBLE);
        } else if (TextUtils.equals("1", user_status)) {
            tvStatus.setText("已实名认证");
            ivDoAuth.setVisibility(View.GONE);
        }
    }

    /**
     * 身份显示不同布局
     */
    private void setVipLayout(boolean isVip) {
        menuIconAdapter.setNewData(getMenuIcon(isVip));
    }

    private List<MenuIconBean> getMenuIcon(boolean isVip) {
        List<MenuIconBean> mList = new ArrayList<>();
        if (isVip) {
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "我的钱包", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "我的红包", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "报告管理", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "订单管理", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "贷款匹配", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "我的收藏", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "邀请好友", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "客户管理", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "客户订单", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "关于鼎鼎", "点击查看详情"));
        } else {
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "报告管理", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "订单管理", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "贷款匹配", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "我的收藏", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "邀请好友", "点击查看详情"));
            mList.add(new MenuIconBean(R.mipmap.icon_yaoqinhaoyou, "关于鼎鼎", "点击查看详情"));
        }
        return mList;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fragment = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
