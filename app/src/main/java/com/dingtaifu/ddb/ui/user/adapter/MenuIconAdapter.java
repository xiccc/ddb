package com.dingtaifu.ddb.ui.user.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.ui.user.bean.MenuIconBean;

import java.util.List;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/5 18:25
 */

public class MenuIconAdapter extends BaseQuickAdapter<MenuIconBean, BaseViewHolder> {

    public MenuIconAdapter(@Nullable List<MenuIconBean> data) {
        super(R.layout.item_menu_icon, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MenuIconBean item) {
        helper.setImageResource(R.id.iv_icon, item.getImg())
                .setText(R.id.tv_name, item.getName())
                .setText(R.id.tv_desc, item.getDesc());
    }
}
