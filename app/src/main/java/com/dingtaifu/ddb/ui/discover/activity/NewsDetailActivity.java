package com.dingtaifu.ddb.ui.discover.activity;

import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.base.BaseActivity;
import com.dingtaifu.ddb.config.AccountHelper;
import com.dingtaifu.ddb.ui.discover.bean.NewsDetailBean;
import com.dingtaifu.ddb.ui.discover.contract.NewsDetailContract;
import com.dingtaifu.ddb.ui.discover.presenter.NewsDetailPresenter;
import com.dingtaifu.ddb.util.Tools;
import com.dingtaifu.ddb.util.ViewUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 发现 - 资讯详情页
 */
public class NewsDetailActivity extends BaseActivity<NewsDetailPresenter> implements NewsDetailContract.View {

    @BindView(R.id.iv_back)
    ImageView ivBack;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_date)
    TextView tvDate;

    @BindView(R.id.iv_zan)
    ImageView ivZan;

    @BindView(R.id.tv_zan)
    TextView tvZan;

    @BindView(R.id.iv_shoucang)
    ImageView ivShoucang;

    @BindView(R.id.webview)
    WebView mWebView;

    private String mStatusShoucang;

    private String id;

    private int mStatusZan = 999;

    @Override
    public int getLayoutId() {
        return R.layout.activity_news_detail;
    }

    @Override
    public void initPresenter() {
        mPresenter.init(this);
    }

    @Override
    public void loadData() {
        id = getIntent().getStringExtra("id");
        if (TextUtils.isEmpty(id)) {
            toast("获取详情id失败！");
            return;
        }
        initView();
        if (AccountHelper.getInstance().getLoginStatus()) {
            mPresenter.getNewsDetail(AccountHelper.getInstance().getUserInfo().getUser_ID(), id, true);
        }

        //点赞状态
        mPresenter.getNewsZanStatus(AccountHelper.getInstance().getUserInfo().getUser_ID(), id);
    }

    private void initView() {
        tvTitle.setText("详情");

        mWebView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        //WebView属性设置！！！
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setDomStorageEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setLoadWithOverviewMode(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setDatabaseEnabled(true);
        //webview在安卓5.0之前默认允许其加载混合网络协议内容
        // 在安卓5.0之后，默认不允许加载http与https混合内容，需要设置webview允许其加载混合网络协议内容
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }

    @OnClick({R.id.iv_back, R.id.iv_zan, R.id.iv_shoucang})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_zan:
                mPresenter.handleZan(AccountHelper.getInstance().getUserInfo().getUser_ID(), id);
                break;
            case R.id.iv_shoucang:
                if (TextUtils.equals("0", mStatusShoucang)) {
                    //未收藏
                    mPresenter.addShouCang(AccountHelper.getInstance().getUserInfo().getUser_ID(), id);
                } else if (TextUtils.equals("1", mStatusShoucang)) {
                    //已收藏
                    mPresenter.delShouCang(AccountHelper.getInstance().getUserInfo().getUser_ID(), id);
                }
                break;
        }
    }

    @Override
    public void showLoading(String content) {
        ViewUtil.loadingContent(this, content);
    }

    @Override
    public void stopLoading() {
        ViewUtil.hideLoading();
    }

    @Override
    public void showErrorMsg(String msg, String type) {
        toast(msg);
    }


    @Override
    public void getDetailSuccess(NewsDetailBean newsDetailBean, boolean isRefresh) {
        tvTitle.setText(newsDetailBean.getTitle());
        tvDate.setText("发布日期：" + Tools.parseTime(newsDetailBean.getDate()));
        tvZan.setText(newsDetailBean.getHit_number() + "人已赞");

        if (isRefresh) {
            mWebView.loadUrl(newsDetailBean.getContent());
        }

        //收藏状态
        mStatusShoucang = newsDetailBean.getExist();
        if (TextUtils.equals("0", mStatusShoucang)) {
            //未收藏
            ivShoucang.setImageResource(R.mipmap.icon_shoucang_n);
        } else if (TextUtils.equals("1", mStatusShoucang)) {
            //已收藏
            ivShoucang.setImageResource(R.mipmap.icon_shoucang_p);
        }
    }

    @Override
    public void addShouCangSuccess() {
        toast("已收藏");
        if (AccountHelper.getInstance().getLoginStatus()) {
            mPresenter.getNewsDetail(AccountHelper.getInstance().getUserInfo().getUser_ID(), id, false);
        }
    }

    @Override
    public void delShouCangSuccess() {
        toast("已取消收藏");
        if (AccountHelper.getInstance().getLoginStatus()) {
            mPresenter.getNewsDetail(AccountHelper.getInstance().getUserInfo().getUser_ID(), id, false);
        }
    }

    @Override
    public void getZanStatus(Integer statusZan) {
        mStatusZan = statusZan;
        if (statusZan == 0) {
            //未赞
            ivZan.setImageResource(R.mipmap.icon_zan_n);
        } else if (statusZan == 1) {
            //已赞
            ivZan.setImageResource(R.mipmap.icon_zan_p);
        }
    }

    @Override
    public void handleZanSuccess(String msg) {
        toast(msg);
        //点赞状态
        mPresenter.getNewsZanStatus(AccountHelper.getInstance().getUserInfo().getUser_ID(), id);
        if (AccountHelper.getInstance().getLoginStatus()) {
            mPresenter.getNewsDetail(AccountHelper.getInstance().getUserInfo().getUser_ID(), id, false);
        }
    }

}
