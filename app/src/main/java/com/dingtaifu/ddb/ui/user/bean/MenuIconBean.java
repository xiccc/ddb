package com.dingtaifu.ddb.ui.user.bean;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/5 18:25
 */

public class MenuIconBean {
    private int    img;
    private String name;
    private String desc;

    public MenuIconBean(int img, String name, String desc) {
        this.img = img;
        this.name = name;
        this.desc = desc;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
