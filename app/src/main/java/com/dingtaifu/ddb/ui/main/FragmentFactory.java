package com.dingtaifu.ddb.ui.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.dingtaifu.ddb.ui.user.fragment.MyFragment;
import com.dingtaifu.ddb.ui.ServiceFragment;
import com.dingtaifu.ddb.ui.discover.fragment.DiscoverFragment;
import com.dingtaifu.ddb.ui.search.fragment.SearchFragment;


public class FragmentFactory {

    private static Fragment lastFragment;

    public enum FragmentStatus {
        None,
        Search,
        Discover,
        Service,
        My
    }

    //    public static void clear() {
    //        if (list != null)
    //            list.clear();
    //        if (manager != null) {
    //            int count = manager.getBackStackEntryCount();
    //            while (count >= 0) {
    //                manager.popBackStackImmediate();
    //                count--;
    //            }
    //        }
    //        manager = null;
    //    }

    public static void changeFragment(FragmentManager manager, FragmentStatus status, int id) {
        FragmentTransaction transaction = manager.beginTransaction();
        Fragment selectFragment = null;
        switch (status) {
            case None:
                return;
            case Search:
                //查询
                selectFragment = SearchFragment.getInstance();
                break;
            case Discover:
                //发现
                selectFragment = DiscoverFragment.getInstance();
                break;
            case Service:
                //服务
                selectFragment = ServiceFragment.getInstance();
                break;
            case My:
                //我的
                selectFragment = MyFragment.getInstance();
                break;
            default:
                break;
        }

        //change
        //        if (list.contains(selectFragment)) {
        //            transaction.hide(lastFragment).show(selectFragment).commitAllowingStateLoss();
        //        } else {
        //            if (list.size() == 0)
        //                transaction.add(id, selectFragment).commitAllowingStateLoss();
        //            else
        //                transaction.hide(lastFragment).add(id, selectFragment).commitAllowingStateLoss();
        //            list.add(selectFragment);
        //        }
        //        lastFragment = selectFragment;

        if (!selectFragment.isAdded()) {
            if (lastFragment != null) {
                transaction.hide(lastFragment);
            }
            transaction.add(id, selectFragment)
                    .commitAllowingStateLoss();
        } else {
            transaction.hide(lastFragment)
                    .show(selectFragment)
                    .commitAllowingStateLoss();
        }
        lastFragment = selectFragment;
    }

}
