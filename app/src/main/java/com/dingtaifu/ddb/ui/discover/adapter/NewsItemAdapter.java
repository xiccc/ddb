package com.dingtaifu.ddb.ui.discover.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.ui.discover.bean.NewsItemBean;

import java.util.List;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/3 11:09
 */

public class NewsItemAdapter extends BaseQuickAdapter<NewsItemBean, BaseViewHolder> {

    public NewsItemAdapter(@Nullable List<NewsItemBean> data) {
        super(R.layout.item_discover_news, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, NewsItemBean item) {
        helper.setText(R.id.tv_title, item.getTitle())
                .setText(R.id.tv_likenum, "点赞数：" + item.getHit_number());
    }
}
