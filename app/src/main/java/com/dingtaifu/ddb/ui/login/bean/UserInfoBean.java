package com.dingtaifu.ddb.ui.login.bean;

import java.io.Serializable;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/3 16:14
 */

public class UserInfoBean implements Serializable {

    /**
     * user_ID : 10
     * user_phone : 17629000032
     * user_password : e10adc3949ba59abbe56e057f20f883e
     * user_email : 17629000032
     * user_status : 1
     * user_level : 0
     * user_invitated_code : FQzdj
     * user_name : 高喜超
     * user_ID_Card : 610527199502186012
     * user_date : 2018-08-31 23:54:25
     * user_total_money : 0.0
     */

    private String user_ID;
    private String user_phone;
    private String user_password;
    private String user_email;
    private String user_status;
    private String user_level;
    private String user_invitated_code;
    private String user_name;
    private String user_ID_Card;
    private String user_date;
    private String user_total_money;

    public String getUser_ID() {
        return user_ID;
    }

    public void setUser_ID(String user_ID) {
        this.user_ID = user_ID;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String getUser_level() {
        return user_level;
    }

    public void setUser_level(String user_level) {
        this.user_level = user_level;
    }

    public String getUser_invitated_code() {
        return user_invitated_code;
    }

    public void setUser_invitated_code(String user_invitated_code) {
        this.user_invitated_code = user_invitated_code;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_ID_Card() {
        return user_ID_Card;
    }

    public void setUser_ID_Card(String user_ID_Card) {
        this.user_ID_Card = user_ID_Card;
    }

    public String getUser_date() {
        return user_date;
    }

    public void setUser_date(String user_date) {
        this.user_date = user_date;
    }

    public String getUser_total_money() {
        return user_total_money;
    }

    public void setUser_total_money(String user_total_money) {
        this.user_total_money = user_total_money;
    }

    @Override
    public String toString() {
        return "UserInfoBean{" +
                "user_ID='" + user_ID + '\'' +
                ", user_phone='" + user_phone + '\'' +
                ", user_password='" + user_password + '\'' +
                ", user_email='" + user_email + '\'' +
                ", user_status='" + user_status + '\'' +
                ", user_level='" + user_level + '\'' +
                ", user_invitated_code='" + user_invitated_code + '\'' +
                ", user_name='" + user_name + '\'' +
                ", user_ID_Card='" + user_ID_Card + '\'' +
                ", user_date='" + user_date + '\'' +
                ", user_total_money='" + user_total_money + '\'' +
                '}';
    }
}
