package com.dingtaifu.ddb.ui.login.contract;

import com.dingtaifu.ddb.base.BaseView;
import com.dingtaifu.ddb.ui.login.bean.UserInfoBean;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/3 16:09
 */

public interface LoginContract {
    interface View extends BaseView {
        void loginSuccess(UserInfoBean userInfoBean);
    }

    interface Presenter {
        void login(String user_phone,
                   String user_password);
    }
}
