package com.dingtaifu.ddb.ui.discover.presenter;

import com.dingtaifu.ddb.base.BasePresenter;
import com.dingtaifu.ddb.http.HttpManager;
import com.dingtaifu.ddb.http.HttpSubscriber;
import com.dingtaifu.ddb.ui.discover.bean.NewsItemBean;
import com.dingtaifu.ddb.ui.discover.contract.DiscoverContract;

import java.util.List;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/3 10:50
 */

public class DiscoverPresenter extends BasePresenter<DiscoverContract.View> implements DiscoverContract
        .Presenter {

    public String TYPE_GET_NEWS_ITEM = "getNewsItems";

    @Override
    public void getNewsItems() {
        toSubscribe(HttpManager.getApi().getNewsItems(), new HttpSubscriber<List<NewsItemBean>>() {
            @Override
            protected void _onStart() {
                mView.showLoading("加载中...");
            }

            @Override
            protected void _onNext(List<NewsItemBean> newsItemBeans) {
                if (newsItemBeans != null) {
                    mView.onGetNewsSuccess(newsItemBeans);
                } else {
                    _onError("获取资讯列表失败！");
                }
            }

            @Override
            protected void _onError(String message) {
                mView.showErrorMsg(message, TYPE_GET_NEWS_ITEM);
            }

            @Override
            protected void _onCompleted() {
                mView.stopLoading();
            }
        });
    }
}
