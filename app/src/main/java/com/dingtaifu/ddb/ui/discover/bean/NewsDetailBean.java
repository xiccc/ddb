package com.dingtaifu.ddb.ui.discover.bean;

/**
 * Description:
 * Author     : Xiccc.
 * Date       : 2018/9/5 10:12
 */

public class NewsDetailBean {

    /**
     * date : 1520404450000
     * exist : 0
     * knowledge_id : 1
     * title : 15条，带你了解个人信用报告
     * type : 0
     * hit_number : 1
     * content : https://mp.weixin.qq.com/s/KoZUQh0f4sQZE3oxsAFpqQ
     */

    private long   date;
    private String exist;
    private int    knowledge_id;
    private String title;
    private int    type;
    private int    hit_number;
    private String content;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getExist() {
        return exist;
    }

    public void setExist(String exist) {
        this.exist = exist;
    }

    public int getKnowledge_id() {
        return knowledge_id;
    }

    public void setKnowledge_id(int knowledge_id) {
        this.knowledge_id = knowledge_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getHit_number() {
        return hit_number;
    }

    public void setHit_number(int hit_number) {
        this.hit_number = hit_number;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
