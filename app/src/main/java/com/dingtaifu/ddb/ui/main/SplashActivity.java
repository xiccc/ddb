package com.dingtaifu.ddb.ui.main;

import android.content.Intent;

import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.base.BaseActivity;
import com.dingtaifu.ddb.config.AccountHelper;
import com.dingtaifu.ddb.ui.login.activity.LoginActivity;

/**
 * 闪屏页
 */
public class SplashActivity extends BaseActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void loadData() {
        if (AccountHelper.getInstance().getLoginStatus()) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
        finish();
    }
}
