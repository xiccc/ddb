package com.dingtaifu.ddb.ui.discover.fragment;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.base.BaseFragment;
import com.dingtaifu.ddb.ui.discover.activity.NewsDetailActivity;
import com.dingtaifu.ddb.ui.discover.adapter.NewsItemAdapter;
import com.dingtaifu.ddb.ui.discover.bean.NewsItemBean;
import com.dingtaifu.ddb.ui.discover.contract.DiscoverContract;
import com.dingtaifu.ddb.ui.discover.presenter.DiscoverPresenter;
import com.dingtaifu.ddb.util.Tools;
import com.dingtaifu.ddb.util.ViewUtil;
import com.dingtaifu.ddb.widget.loading.LoadingLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 首页 - 发现
 */

public class DiscoverFragment extends BaseFragment<DiscoverPresenter> implements DiscoverContract.View {

    @BindView(R.id.tv_big_content)
    TextView tvBigTitle;

    @BindView(R.id.tv_small_content)
    TextView tvSmallTitle;

    @BindView(R.id.rv_news)
    RecyclerView rvNews;

    @BindView(R.id.loadingLayout)
    LoadingLayout mLoadingLayout;

    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private List<NewsItemBean> mNewsItemBeans = new ArrayList<>();


    public static DiscoverFragment fragment;
    private       NewsItemAdapter  newsItemAdapter;


    public static DiscoverFragment getInstance() {
        if (fragment == null)
            fragment = new DiscoverFragment();
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_discover;
    }

    @Override
    public void initPresenter() {
        mPresenter.init(this);
    }

    @Override
    public void loadData() {
        initView();
        initListener();
    }

    @Override
    public void onResume() {
        super.onResume();
        //加载数据
        mPresenter.getNewsItems();
    }

    private void initListener() {

        newsItemAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (Tools.isFastDoubleClick()) {
                    return;
                }
                String knowledge_id = newsItemAdapter.getData().get(position).getKnowledge_id();
                startActivity(new Intent(mActivity, NewsDetailActivity.class)
                        .putExtra("id", knowledge_id));
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getNewsItems();
            }
        });
    }

    private void initView() {
        tvBigTitle.setText("发现");
        tvSmallTitle.setText("随时随地开启信用之旅");
        rvNews.setLayoutManager(new LinearLayoutManager(mContext));
        newsItemAdapter = new NewsItemAdapter(mNewsItemBeans);
        View ivHeadView = getLayoutInflater().inflate(R.layout.headview_discover_banner, null);
        newsItemAdapter.addHeaderView(ivHeadView);
        rvNews.setAdapter(newsItemAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fragment = null;
    }

    @Override
    public void showLoading(String content) {
        mLoadingLayout.showLoading(mActivity, content);
    }

    @Override
    public void stopLoading() {
        ViewUtil.hideLoading();
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void showErrorMsg(String msg, String type) {
        toast(msg);
        mLoadingLayout.handleServerError(msg);
        if (TextUtils.equals(type, mPresenter.TYPE_GET_NEWS_ITEM)) {
            mLoadingLayout.handleServerError(msg);
            mLoadingLayout.setOnReloadListener(new LoadingLayout.OnReloadListener() {
                @Override
                public void onReload(View v) {
                    mPresenter.getNewsItems();
                }
            });
        }
    }

    @Override
    public void onGetNewsSuccess(List<NewsItemBean> newsItemBeans) {
        mLoadingLayout.loadingSuccess();

        mNewsItemBeans.clear();
        mNewsItemBeans.addAll(newsItemBeans);
        newsItemAdapter.setNewData(mNewsItemBeans);
    }
}
