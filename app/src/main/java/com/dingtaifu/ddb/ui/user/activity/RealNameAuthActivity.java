package com.dingtaifu.ddb.ui.user.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.base.BaseActivity;
import com.dingtaifu.ddb.config.AccountHelper;
import com.dingtaifu.ddb.event.RealNameAuthEvent;
import com.dingtaifu.ddb.ui.login.bean.UserInfoBean;
import com.dingtaifu.ddb.ui.user.contract.RealNameAuthContract;
import com.dingtaifu.ddb.ui.user.presenter.RealNameAuthPresenter;
import com.dingtaifu.ddb.util.ViewUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 实名认证
 */
public class RealNameAuthActivity extends BaseActivity<RealNameAuthPresenter> implements RealNameAuthContract.View {

    @BindView(R.id.iv_back)
    ImageView ivBack;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.et_name)
    EditText etName;

    @BindView(R.id.et_card)
    EditText etCard;

    @BindView(R.id.tv_do_auth)
    TextView tvDoAuth;

    @BindView(R.id.tv_rule_zhegnxin)
    TextView tvRuleZhegnxin;

    @Override
    public int getLayoutId() {
        return R.layout.activity_real_name_auth;
    }

    @Override
    public void initPresenter() {
        mPresenter.init(this);
    }

    @Override
    public void loadData() {
        initView();
    }

    private void initView() {
        tvTitle.setText("实名认证");
    }

    @OnClick({R.id.iv_back, R.id.tv_do_auth, R.id.tv_rule_zhegnxin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_do_auth:
                String realName = etName.getText().toString().trim();
                String card = etCard.getText().toString().trim();
                if (TextUtils.isEmpty(realName)) {
                    toast("请输入姓名！");
                } else if (TextUtils.isEmpty(card)) {
                    toast("请输入身份证号！");
                } else {
                    mPresenter.doRealNameAuth(AccountHelper.getInstance().getUserInfo().getUser_phone(),
                            AccountHelper.getInstance().getUserInfo().getUser_email(), realName, card);
                }
                break;
            case R.id.tv_rule_zhegnxin:
                toast("协议");
                break;
        }
    }

    @Override
    public void showLoading(String content) {
        ViewUtil.loadingContent(this, content);
    }

    @Override
    public void stopLoading() {
        ViewUtil.hideLoading();
    }

    @Override
    public void showErrorMsg(String msg, String type) {
        toast(msg);
    }

    @Override
    public void realNameAuthSuccess(UserInfoBean userInfoBean) {
        //认证成功
        //更新用户信息
        AccountHelper.getInstance().setUserInfo(userInfoBean);
        //刷新数据等
        EventBus.getDefault().post(new RealNameAuthEvent(userInfoBean));
        finish();
    }
}
