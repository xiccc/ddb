package com.dingtaifu.ddb.ui;

import android.widget.TextView;

import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.base.BaseFragment;

import butterknife.BindView;

/**
 * 首页 - 服务
 */

public class ServiceFragment extends BaseFragment {

    @BindView(R.id.tv_big_content)
    TextView tvBigTitle;

    @BindView(R.id.tv_small_content)
    TextView tvSmallTitle;

    public static ServiceFragment fragment;

    public static ServiceFragment getInstance() {
        if (fragment == null)
            fragment = new ServiceFragment();
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_service;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void loadData() {
        initView();
    }

    private void initView() {
        tvBigTitle.setText("服务");
        tvSmallTitle.setText("解答您使用中遇到的问题");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fragment = null;
    }

}
