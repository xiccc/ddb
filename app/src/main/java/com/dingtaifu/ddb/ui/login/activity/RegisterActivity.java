package com.dingtaifu.ddb.ui.login.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.base.BaseActivity;
import com.dingtaifu.ddb.ui.login.contract.RegisterContract;
import com.dingtaifu.ddb.ui.login.presenter.RegisterPresenter;
import com.dingtaifu.ddb.util.CountDownTimerUtils;
import com.dingtaifu.ddb.util.RegexUtils;
import com.dingtaifu.ddb.util.ViewUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 注册
 */
public class RegisterActivity extends BaseActivity<RegisterPresenter> implements RegisterContract.View {

    @BindView(R.id.iv_back)
    ImageView ivBack;


    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.et_phone)
    EditText etPhone;

    @BindView(R.id.et_checkcode)
    EditText etCheckcode;

    @BindView(R.id.tv_send_checkcode)
    TextView tvSendCheckcode;

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.et_invitecode)
    EditText etInvitecode;

    @BindView(R.id.tv_register)
    TextView tvRegister;

    @BindView(R.id.tv_rule_register)
    TextView tvRuleRegister;

    private CountDownTimerUtils countDownTimerUtils;

    private String code_key;


    @Override
    public int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    public void initPresenter() {
        mPresenter.init(this);
    }

    @Override
    public void loadData() {
        initView();
    }

    private void initView() {
        tvTitle.setText("手机号注册");
        countDownTimerUtils = new CountDownTimerUtils(tvSendCheckcode, 90000, 1000);
    }


    @OnClick({R.id.iv_back, R.id.tv_send_checkcode, R.id.tv_register, R.id.tv_rule_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_send_checkcode:
                //发送验证码
                String phone1 = etPhone.getText().toString().trim();
                if (TextUtils.isEmpty(phone1)) {
                    toast("请输入手机号！");
                } else if (!RegexUtils.isMobileExact(phone1)) {
                    toast("手机号格式不正确！");
                } else {
                    mPresenter.sendPhoneCheckCode(phone1);
                }
                break;
            case R.id.tv_register:
                //注册
                String phone = etPhone.getText().toString().trim();
                String checkcode = etCheckcode.getText().toString().trim();
                String password = etPassword.getText().toString().trim();
                String invitecode = etInvitecode.getText().toString().trim();

                if (TextUtils.isEmpty(phone)) {
                    toast("请输入手机号！");
                } else if (!RegexUtils.isMobileExact(phone)) {
                    toast("手机号格式不正确！");
                } else if (TextUtils.isEmpty(checkcode)) {
                    toast("验证码不能为空！");
                } else if (TextUtils.isEmpty(password)) {
                    toast("请输入密码！");
                } else {
                    mPresenter.register(phone, password, invitecode, code_key, checkcode);
                }

                break;
            case R.id.tv_rule_register:
                toast("用户协议");
                break;
        }
    }

    @Override
    public void showLoading(String content) {
        ViewUtil.loadingContent(this, content);
    }

    @Override
    public void stopLoading() {
        ViewUtil.hideLoading();
    }

    @Override
    public void showErrorMsg(String msg, String type) {
        toast(msg);
    }

    @Override
    public void registerSuccess() {
        toast("注册成功");
        finish();
    }

    @Override
    public void sendCheckCodeSuccess(String code_key) {
        toast("验证码发送成功！");
        countDownTimerUtils.start();
        this.code_key = code_key;
    }
}
