package com.dingtaifu.ddb.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dingtaifu.ddb.util.TUtil;
import com.dingtaifu.ddb.util.TitleUtil;

import butterknife.ButterKnife;

/**
 * fragment基类
 */

public abstract class BaseFragment<T extends BasePresenter> extends Fragment {
    protected View         mView;
    public    T            mPresenter;
    public    Context      mContext;
    public    BaseActivity mActivity;
    protected TitleUtil    mTitle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        mView = inflater.inflate(getLayoutId(), null);
        ButterKnife.bind(this, mView);
        mContext = getContext();
        mActivity = (BaseActivity) getActivity();
        mPresenter = TUtil.getT(this, 0);
        if (mPresenter != null) {
            //mPresenter.mContext = mContext;
        }
        mTitle = new TitleUtil(mActivity, mView);
        initPresenter();
        loadData();
        return mView;
    }

    /*********************
     * 子类实现
     *****************************/
    //获取布局文件
    public abstract int getLayoutId();

    //简单页面无需mvp就不用管此方法即可,完美兼容各种实际场景的变通
    public abstract void initPresenter();

    //加载View、设置数据
    public abstract void loadData();

    /**
     * 请求权限封装
     * @param permissions
     * @param listener
     */
    public void requestPermissions(String[] permissions, PermissionsListener listener) {
        mActivity.requestPermissions(permissions, listener);
    }

    public void toast(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) {
            mPresenter.onDestroy();
        }
    }
}
