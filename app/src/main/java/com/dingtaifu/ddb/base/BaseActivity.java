package com.dingtaifu.ddb.base;


import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Toast;

import com.dingtaifu.ddb.R;
import com.dingtaifu.ddb.app.AppManager;
import com.dingtaifu.ddb.util.StatusBarUtil;
import com.dingtaifu.ddb.util.TUtil;
import com.dingtaifu.ddb.util.TitleUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;


/**
 * 基类
 */
public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity {
    public    T                   mPresenter;
    public    Context             mContext;
    public    BaseActivity        mActivity;
    private   PermissionsListener mListener;
    protected TitleUtil           mTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        AppManager.getInstance().addActivity(this);
        ButterKnife.bind(this);
        mContext = this;
        mActivity = this;
        mPresenter = TUtil.getT(this, 0);
        mTitle = new TitleUtil(this, getWindow().getDecorView());
        initStatusBar();
        initPresenter();
        loadData();
    }

    private void initStatusBar() {
        //层垫式状态栏
        StatusBarUtil.setStatusBarColor(this, R.color.titleBackbround);
    }


    /*********************
     * 子类实现
     *****************************/
    //获取布局文件
    public abstract int getLayoutId();

    //简单页面无需mvp就不用管此方法即可,完美兼容各种实际场景的变通
    public abstract void initPresenter();

    //加载、设置数据
    public abstract void loadData();

    /**
     * 请求权限封装
     * @param permissions
     * @param listener
     */
    public void requestPermissions(String[] permissions, PermissionsListener listener) {
        mListener = listener;
        List<String> requestPermissions = new ArrayList<>();
        if (permissions != null) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED) {
                    requestPermissions.add(permission);
                }
            }
        }
        if (!requestPermissions.isEmpty() && Build.VERSION.SDK_INT >= 23) {
            ActivityCompat.requestPermissions(this, requestPermissions.toArray(new String[requestPermissions.size()])
                    , 1);
        } else {
            mListener.onGranted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                List<String> deniedPermissions = new ArrayList<>();
                //当所有拒绝的权限都勾选不再询问，这个值为true,这个时候可以引导用户手动去授权。
                boolean isNeverAsk = true;
                for (int i = 0; i < grantResults.length; i++) {
                    int grantResult = grantResults[i];
                    String permission = permissions[i];
                    if (grantResult == PackageManager.PERMISSION_DENIED) {
                        deniedPermissions.add(permissions[i]);
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) { // 点击拒绝但没有勾选不再询问
                            isNeverAsk = false;
                        }
                    }
                }
                if (deniedPermissions.isEmpty()) {
                    try {
                        mListener.onGranted();
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                        mListener.onDenied(Arrays.asList(permissions), true);
                    }
                } else {
                    mListener.onDenied(deniedPermissions, isNeverAsk);
                }
                break;
            default:
                break;
        }
    }

    // 启动应用的设置弹窗
    //    public void toAppSettings(String message, final boolean isFinish) {
    //        if (TextUtils.isEmpty(message)) {
    //            message = "\"" + MyApp.getContext().getPackageName() + "\"缺少必要权限";
    //        }
    //        AlertFragmentDialog.Builder builder = new AlertFragmentDialog.Builder(this);
    //        if (isFinish) {
    //            builder.setLeftBtnText("退出")
    //                    .setLeftCallBack(new AlertFragmentDialog.LeftClickCallBack() {
    //                        @Override
    //                        public void dialogLeftBtnClick() {
    //                            finish();
    //                        }
    //                    });
    //        } else {
    //            builder.setLeftBtnText("取消");
    //        }
    //        builder.setContent(message + "\n请手动授予\"" + App.getAPPName() + "\"访问您的权限")
    //                .setRightBtnText("去设置")
    //                .setRightCallBack(new AlertFragmentDialog.RightClickCallBack() {
    //                    @Override
    //                    public void dialogRightBtnClick() {
    //                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
    //                        intent.setData(Uri.parse("package:" + getPackageName()));
    //                        startActivity(intent);
    //                    }
    //                }).build();
    //    }


    public void toast(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.onDestroy();
        }
        AppManager.getInstance().finishActivity(this);
    }

}
